# OpenML dataset: 9_Tumors

https://www.openml.org/d/45083

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**9 Tumors dataset**

**Authors**: Staunton, J.E., Slonim, D.K., Coller, H.A., Tamayo, P., Angelo, M.J., Park, J., Scherf, U., Lee, J.K., Reinhold, W.O. et al

**Please cite**: ([URL](https://www.scopus.com/record/display.uri?eid=2-s2.0-0035845531&origin=inward)): J. Staunton, D. Slonim, H. Coller, P. Tamayo, M. Angelo, J. Park, U. Scherf, J. Lee, W. Reinhold, J. Weinstein, et al, Chemosensitivity prediction by transcriptional profiling, Proc. Nat. Acad. Sci. 98 (19) (2001) 10787-10792

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45083) of an [OpenML dataset](https://www.openml.org/d/45083). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45083/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45083/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45083/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

